const makeList = (cities, list = document.body) => {
	cities = ["Kyiv", "Bila Tserkva", "Kharkiv", "Odesa", "Lviv"];
	const listHtml = cities.map(elem => `<li>${elem}</li>`);
	list = document.createElement('ul');
	document.body.append(list);
	list.innerHTML = `${listHtml.join('')}`;
}
makeList();


